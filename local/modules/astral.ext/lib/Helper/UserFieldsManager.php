<?php

/** @noinspection PhpUnused */

namespace Astral\Ext\Helper;

use Bitrix\Main\ArgumentNullException;
use Bitrix\Main\Localization\Loc;
use CUserFieldEnum;
use CUserTypeEntity;

final class UserFieldsManager
{
	private static array $arUfFields = [];

	/**
	 * @param false $showError
	 *
	 * @throws ArgumentNullException
	 */
	public static function check(bool $showError = false): void
	{
		global $APPLICATION;
		$arError = [];
		$path = dirname(__DIR__, 2) . '/userFields';
		if (is_dir($path))
		{
			$dir = opendir($path);
			while ($fileName = readdir($dir))
			{
				if (
					file_exists($path . '/' . $fileName)
					&& strpos($fileName, '.php') > 0)
				{
					$arFields = require($path . '/' . $fileName);
					if (!empty($arFields) && is_array($arFields))
					{
						$arError = self::creatingFields($arFields);
					}
				}
			}
		}
		if ($showError === false)
		{
			$APPLICATION->ResetException();
		}
		if (!empty($arError))
		{
			echo '<pre>';
			foreach ($arError as $error)
			{
				ShowError(print_r($error, true));
			}
			echo '</pre>';
		}
	}

	/**
	 * @param array $arFields
	 *
	 * @return array
	 *
	 * @throws ArgumentNullException
	 */
	private static function creatingFields(array $arFields): array
	{
		global $APPLICATION;
		$arError = [];
		$oUserTypeEntity = new CUserTypeEntity();
		foreach ($arFields as $field)
		{
			$arUserFields = self::getFields($field['ENTITY_ID']);
			$field = self::getDefault($field);
			if (isset($arUserFields[$field['FIELD_NAME']]['ID']))
			{
				unset($field['USER_TYPE_ID'], $field['MULTIPLE']);
				$id = $oUserTypeEntity->Update($arUserFields[$field['FIELD_NAME']]['ID'], $field);
			} else
			{
				$id = $oUserTypeEntity->Add($field);
				if ($id > 0 && $field['USER_TYPE_ID'] === 'enumeration'
					&& !empty($field['_ITEMS']))
				{
					$i = 0;
					$items = [];
					foreach ($field['_ITEMS'] as $item)
					{
						if (!isset($item['SORT']))
						{
							$item['SORT'] = 10 * ($i + 1);
						}
						if (!isset($item['DEF']))
						{
							$items['DEF'] = 'N';
						}
						if (!isset($item['XML_ID']))
						{
							$items['XML_ID'] = md5($item['VALUE']);
						}
						$items['n' . $i] = $item;
						$i++;
					}
					$obEnum = new CUserFieldEnum();
					$obEnum->SetEnumValues($id, $items);
				}
			}
			if ($id === false)
			{
				$arError[] = [
					'Error' => $APPLICATION->GetException(),
					'Context' => 'CUserTypeEntity::Add/CUserTypeEntity::Add',
					'Data' => $field,
				];
			}
		}
		return $arError;
	}

	/**
	 * @param string $entityId
	 *
	 * @return array
	 */
	private static function getFields(string $entityId): array
	{
		if (!isset(self::$arUfFields[$entityId]))
		{
			global $USER_FIELD_MANAGER;
			self::$arUfFields[$entityId] = $USER_FIELD_MANAGER->GetUserFields($entityId, 0, LANGUAGE_ID);
		}
		return self::$arUfFields[$entityId];
	}

	/**
	 * @param array $params
	 *
	 * @return array
	 *
	 * @throws ArgumentNullException
	 */
	private static function getDefault(array $params): array
	{
		if (!isset($params['ENTITY_ID']))
		{
			throw new ArgumentNullException('ENTITY_ID');
		}
		if (!isset($params['FIELD_NAME']))
		{
			throw new ArgumentNullException('FIELD_NAME');
		}
		if (!isset($params['TITLE']))
		{
			$params['TITLE'] = Loc::getMessage($params['FIELD_NAME']);
		}
		$result = array_merge([
			'USER_TYPE_ID' => 'string',
			'XML_ID' => '',
			'SORT' => 1000,
			'MULTIPLE' => 'N',
			'MANDATORY' => 'N',
			'SHOW_FILTER' => 'E',
			'SHOW_IN_LIST' => 'Y',
			'EDIT_IN_LIST' => 'Y',
			'IS_SEARCHABLE' => 'N',
			'SETTINGS' => [],
			'EDIT_FORM_LABEL' => [
				'ru' => $params['TITLE'],
			],
			'LIST_COLUMN_LABEL' => [
				'ru' => $params['TITLE'],
			],
			'LIST_FILTER_LABEL' => [
				'ru' => $params['TITLE'],
			],
			'ERROR_MESSAGE' => [
				'ru' => $params['TITLE'],
			],
			'HELP_MESSAGE' => [
				'ru' => $params['TITLE'],
			],
		], $params);
		if ($result['USER_TYPE_ID'] === 'boolean')
		{
			$result['SETTINGS']['LABEL_CHECKBOX'] = $params['TITLE'];
		}
		if (!isset($params['SHOW_FILTER']))
		{
			switch ($result['USER_TYPE_ID'])
			{
				case 'crm_status':
				case 'crm':
				case 'employee':
				case 'iblock_element':
					$result['SHOW_FILTER'] = 'I';
					break;
				default:
					$result['SHOW_FILTER'] = 'E';
					break;
			}
		}
		return $result;
	}

	public static function getItems(string $items): array
	{
		$result = [];
		foreach (explode("\n", $items) as $item)
		{
			$result[] = [
				'VALUE' => trim($item),
			];
		}
		return $result;
	}
}
