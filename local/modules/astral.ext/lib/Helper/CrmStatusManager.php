<?php

/** @noinspection PhpUnused */

namespace Astral\Ext\Helper;

use Bitrix\Main\Config\Option;
use CCrmStatus;
use Exception;

final class CrmStatusManager
{

	/**
	 * @param false $showError
	 *
	 */
	public static function check(bool $showError = false): void
	{
		global $APPLICATION;
		$arError = [];
		$path = dirname(__DIR__, 2) . '/crmStatus';
		if (is_dir($path))
		{
			$dir = opendir($path);
			while ($fileName = readdir($dir))
			{
				if (
					file_exists($path . '/' . $fileName)
					&& strpos($fileName, '.php') > 0)
				{
					$arFields = require($path . '/' . $fileName);
					$configId = 'crm_status_' . mb_strtolower(basename($fileName, '.php'));
					if (!empty($arFields['CONFIG_VERSION']) && !empty($arFields['ENTITY_ID']) && !empty($arFields['STATUS_LIST'])
						&& ((string) Option::get('astral.ext', $configId) !== (string) $arFields['CONFIG_VERSION']))
					{
						try
						{
							$arNewStatusGroupSemantic = [];
							foreach ($arFields['STATUS_LIST'] as $key => $status)
							{
								$arNewStatusGroupSemantic[$status['SEMANTICS']][$key] = $status;
							}
							$arCurrentStatusGroupSemantic = [];
							$crmStatus = new CCrmStatus($arFields['ENTITY_ID']);
							$resStatus = $crmStatus::GetList(
								[
									'SORT' => 'ASC',
								],
								[
									'ENTITY_ID' => $arFields['ENTITY_ID'],
								]
							);
							while ($status = $resStatus->Fetch())
							{
								$arCurrentStatusGroupSemantic[$status['SEMANTICS']][$status['STATUS_ID']] = $status;
							}
							foreach ($arNewStatusGroupSemantic as $semantic => $statuses)
							{
								$indexSort = 0;
								foreach ($statuses as $status)
								{
									$indexSort = (int) $status['SORT'];
									if (array_key_exists($status['STATUS_ID'], $arCurrentStatusGroupSemantic[$semantic]))
									{
										$updateFieldsStatus = [];
										if (trim($arCurrentStatusGroupSemantic[$semantic][$status['STATUS_ID']]['NAME']) !== trim($status['NAME']))
										{
											$updateFieldsStatus['NAME'] = $status['NAME'];
										}
										if ((int) $arCurrentStatusGroupSemantic[$semantic][$status['STATUS_ID']]['SORT'] !== $indexSort)
										{
											$updateFieldsStatus['SORT'] = $indexSort;
										}
										if (!empty($updateFieldsStatus))
										{
											$updateStatus = $crmStatus->Update($arCurrentStatusGroupSemantic[$semantic][$status['STATUS_ID']]['ID'], $updateFieldsStatus);
											if (!$updateStatus)
											{
												$arError[] = $crmStatus->GetLastError();
											}
										}
										unset($arCurrentStatusGroupSemantic[$semantic][$status['STATUS_ID']]);
									} else
									{
										$addStatus = $crmStatus->Add($status);
										if (!$addStatus)
										{
											$arError[] = $crmStatus->GetLastError();
										}
									}
								}
								if (!empty($arCurrentStatusGroupSemantic[$semantic]))
								{
									foreach ($arCurrentStatusGroupSemantic[$semantic] as $status)
									{
										$indexSort += 10;
										$updateStatus = $crmStatus->Update($status['ID'], [
											'SORT' => $indexSort,
										]);
										if (!$updateStatus)
										{
											$arError[] = $crmStatus->GetLastError();
										}
									}
								}
							}
							Option::set('astral.ext', $configId, $arFields['CONFIG_VERSION']);
						} catch (Exception $exception)
						{
							$arError[] = $exception->getMessage();
						}
					}
				}
			}
		}
		if ($showError === false)
		{
			$APPLICATION->ResetException();
		}
		if (!empty($arError))
		{
			echo '<pre>';
			foreach ($arError as $error)
			{
				ShowError(print_r($error, true));
			}
			echo '</pre>';
		}
	}
}
