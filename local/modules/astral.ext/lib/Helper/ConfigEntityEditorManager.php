<?php

/** @noinspection PhpUnused */

namespace Astral\Ext\Helper;

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\UI\Form\EntityEditorConfigScope;
use Bitrix\UI\Form\EntityEditorConfiguration;
use Exception;

final class ConfigEntityEditorManager
{

	/**
	 * @param false $showError
	 *
	 * @throws LoaderException
	 */
	public static function check(bool $showError = false): void
	{
		global $APPLICATION;
		$arError = [];
		$path = dirname(__DIR__, 2) . '/entityEditorConfig';
		if (is_dir($path) && Loader::includeModule('crm'))
		{
			$dir = opendir($path);
			while ($fileName = readdir($dir))
			{
				if (
					file_exists($path . '/' . $fileName)
					&& strpos($fileName, '.php') > 0)
				{
					$arField = require($path . '/' . $fileName);
					if (!empty($arField) && is_array($arField))
					{
						try
						{
							$config = new EntityEditorConfiguration($arField['categoryName']);
							$scope = $arField['scope'] ?? EntityEditorConfigScope::PERSONAL;
							$arConfigExist = $config->get($arField['configID'], $scope);
							if ($arField['config'][0]['astral'] !== $arConfigExist[0]['astral'])
							{
								$config->set(
									$arField['configID'],
									$arField['config'],
									[
										'scope' => $scope,
									],
								);
							}
						} catch (Exception $exception)
						{
							$arError[] = [
								'Error' => $exception->getMessage(),
								'Context' => 'EntityEditorConfiguration:set',
								'Data' => $arField,
							];
						}
					}
				}
			}
		}
		if ($showError === false)
		{
			$APPLICATION->ResetException();
		}
		if (!empty($arError))
		{
			echo '<pre>';
			foreach ($arError as $error)
			{
				ShowError(print_r($error, true));
			}
			echo '</pre>';
		}
	}
}
