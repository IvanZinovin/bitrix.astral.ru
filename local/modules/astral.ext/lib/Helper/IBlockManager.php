<?php

namespace Astral\Ext\Helper;

use Bitrix\Iblock\Iblock;
use Bitrix\Iblock\IblockTable;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use CIBlock;
use CIBlockElement;
use CIBlockSection;
use CList;
use Exception;
use RuntimeException;

final class IBlockManager
{
	private static array $iBlocks;
	private static int $sort;

	/**
	 * Создание пользовательских полей из файлов
	 * /userIBlocks
	 *
	 * @param bool $showError
	 *
	 * @throws ArgumentException
	 * @throws LoaderException
	 * @throws ObjectPropertyException
	 * @throws SystemException
	 */
	public static function check(bool $showError = false): void
	{
		$arError = [];
		$path = dirname(__DIR__, 2) . '/userIBlocks';
		if (is_dir($path)
			&& Loader::includeModule('iblock')
			&& Loader::includeModule('lists'))
		{
			$dir = opendir($path);
			while ($fileName = readdir($dir))
			{
				if (
					file_exists($path . '/' . $fileName)
					&& strpos($fileName, '.php') > 0)
				{
					$arConfigIBlock = require($path . '/' . $fileName);
					if (is_array($arConfigIBlock) && !empty($arConfigIBlock))
					{
						$arError = self::creatingIBock($arConfigIBlock);
					}
				}
			}
		}
		if ($showError === true)
		{
			echo '<pre>';
			foreach ($arError as $error)
			{
				ShowError(print_r($error, true));
			}
			echo '</pre>';
		}
		unset($arError);
	}

	/**
	 * @param array $arConfigIBlock
	 *
	 * @return array
	 *
	 * @throws ArgumentException
	 * @throws ObjectPropertyException
	 * @throws SystemException
	 */
	private static function creatingIBock(array $arConfigIBlock): array
	{
		$ib = new CIBlock();
		$arError = [];
		self::$sort = 0;
		[$arItems, $arFields, $arSections, $arConfigIBlock] = self::processing($arConfigIBlock);

		$idIBlock = (int) $ib->Add($arConfigIBlock);
		$isNewBlock = $idIBlock !== 0;
		if ($idIBlock === 0)
		{
			$arError[] = [
				'Error' => $ib->LAST_ERROR,
				'Context' => 'CIBlock::Add',
				'Data' => $arConfigIBlock,
			];
		}
		$arIBlockCode = self::getIdFormCode([$arConfigIBlock['API_CODE']]);
		$idIBlock = (int) $arIBlockCode[$arConfigIBlock['API_CODE']];
		if ($idIBlock > 0)
		{
			$arSectionCode = [];
			if (!empty($arFields))
			{
				$obList = new CList($idIBlock);
				$arFieldsExist = $obList->GetFields();
				self::$sort = max(array_column($arFieldsExist, 'SORT'));
				$arFieldsExistCode = [];
				foreach ($arFieldsExist as $key => $item)
				{
					$arFieldsExistCode[$item['CODE'] ?? $key] = $item;
				}
				unset($arFieldsExist);
				foreach ($arFields as $arField)
				{
					$arField = self::getDefaultField($arField);
					try
					{
						if (isset($arFieldsExistCode[$arField['CODE']]))
						{
							$arField['SORT'] = $arFieldsExistCode[$arField['CODE']]['SORT'];
							unset($arField['LIST']);
							$obList->UpdateField($arFieldsExistCode[$arField['CODE']], $arField);
						} else
						{

							$id = $obList->AddField($arField);
							//HotFix
							if ($id === false && mb_strpos($arField['TYPE'], ':') !== false)
							{
								$type = $arField['TYPE'];
								$arField['TYPE'] = 'S';
								$id = $obList->AddField($arField);
								$arField['TYPE'] = $type;
								$obList->UpdateField($id, $arField);
							}
						}
					} catch (Exception $exception)
					{
						$arError[] = [
							'Error' => $exception->getMessage(),
							'Context' => 'CList::AddField/CList::UpdateField',
							'Data' => $arField,
						];
					}
				}
				$obList->Save();
			}
			if ($isNewBlock)
			{
				if (!empty($arSections))
				{
					$bs = new CIBlockSection();
					foreach ($arSections as $arSection)
					{
						$arSection['IBLOCK_ID'] = $idIBlock;
						$idSection = $bs->Add($arSection);
						if ($idSection > 0 && !empty($arSection['XML_ID']))
						{
							$arSectionCode[$arSection['XML_ID']] = $idSection;
						} else
						{
							$arError[] = [
								'Error' => $bs->LAST_ERROR,
								'Context' => 'CIBlockSection::Add',
								'Data' => $arSection,
							];
						}
					}
				}
				if (!empty($arItems))
				{
					$el = new CIBlockElement();
					foreach ($arItems as $arElement)
					{
						$sectionCode = $arElement['_SECTION_XML'];
						if (isset($sectionCode))
						{
							$arElement['IBLOCK_SECTION_ID'] = $arSectionCode[$sectionCode];
							unset($arElement['_SECTION_XML']);
						}
						$arElement['IBLOCK_ID'] = $idIBlock;
						$idElement = $el->Add($arElement);
						if ($idElement === 0)
						{
							$arError[] = [
								'Error' => $el->LAST_ERROR,
								'Context' => 'CIBlockElement::Add',
								'Data' => $arElement,
							];
						}
					}
				}
			}
		}
		return $arError;
	}

	/**
	 * @param array $arConfigIBlock
	 *
	 * @return array
	 */
	private static function processing(array $arConfigIBlock): array
	{
		$arItems = $arConfigIBlock['_ITEMS'];
		$arFields = $arConfigIBlock['_FIELDS'];
		$arSections = $arConfigIBlock['_SECTION'];
		unset(
			$arConfigIBlock['_ITEMS'],
			$arConfigIBlock['_FIELDS'],
			$arConfigIBlock['_SECTION']
		);
		$arConfigIBlock = self::addConfigBizProc($arConfigIBlock);
		return [$arItems, $arFields, $arSections, $arConfigIBlock];
	}

	/**
	 * @param array $arConfigIBlock
	 *
	 * @return array
	 */
	private static function addConfigBizProc(array $arConfigIBlock): array
	{
		if (!isset($arConfigIBlock['WF_TYPE']))
		{
			$arConfigIBlock['WF_TYPE'] = 'N';
		}
		if (!isset($arConfigIBlock['BIZPROC']))
		{
			$arConfigIBlock['BIZPROC'] = 'N';
		}
		if (!isset($arConfigIBlock['RIGHTS_MODE']))
		{
			$arConfigIBlock['RIGHTS_MODE'] = 'E';
		}
		if (!isset($arConfigIBlock['WORKFLOW']))
		{
			$arConfigIBlock['WORKFLOW'] = 'N';
		}
		return $arConfigIBlock;
	}

	/**
	 * @param array $arCode
	 *
	 * @return array
	 *
	 * @throws ArgumentException
	 * @throws ObjectPropertyException
	 * @throws SystemException
	 */
	public static function getIdFormCode(array $arCode): array
	{
		if (empty($arCode))
		{
			throw new RuntimeException('empty $arCode');
		}
		return array_column(IblockTable::getList([
			'filter' => [
				'CODE' => $arCode,
			],
			'select' => [
				'ID',
				'CODE',
			],
		])->fetchAll(), 'ID', 'CODE');
	}

	/**
	 * @param array $field
	 *
	 * @return array
	 */
	private static function getDefaultField(array $field): array
	{
		self::$sort += 10;
		return array_merge(
			[
				'SORT' => self::$sort,
				'NAME' => 'NAME',
				'IS_REQUIRED' => 'N',
				'MULTIPLE' => 'N',
				'CODE' => 'CODE',
				'TYPE' => 'S',
				'DEFAULT_VALUE' => '',
				'USER_TYPE_SETTINGS' => [],
				'SETTINGS' => [
					'SHOW_ADD_FORM' => 'Y',
					'SHOW_EDIT_FORM' => 'Y',
				],
			],
			$field
		);
	}

	/**
	 * @param string $apiCode
	 *
	 * @return Iblock|null
	 *
	 * @throws ArgumentException
	 * @throws LoaderException
	 * @throws ObjectPropertyException
	 * @throws SystemException
	 */
	public static function getIBlock(string $apiCode): ?Iblock
	{
		if (!array_key_exists($apiCode, self::$iBlocks) && Loader::includeModule('iblock'))
		{
			$entity = IblockTable::compileEntity($apiCode);
			if ($entity !== false)
			{
				self::$iBlocks[$apiCode] = $entity->getIblock();
			} else
			{
				self::$iBlocks[$apiCode] = null;
			}
		}
		return self::$iBlocks[$apiCode];
	}

	public static function getEnumList(string $items): array
	{
		$result = [];
		$i = 0;
		foreach (explode("\n", $items) as $item)
		{
			$item = trim($item);
			if ($item !== '')
			{
				$result["n$i"] = [
					'XML_ID' => md5($item),
					'SORT' => 10 * ($i + 1),
					'VALUE' => trim($item),
				];
				$i++;
			}
		}
		return $result;
	}

	/**
	 * @param string $items
	 *
	 * @return array
	 */
	public static function getItemList(string $items): array
	{
		$result = [];
		foreach (explode("\n", $items) as $item)
		{
			$item = trim($item);
			if ($item !== '')
			{
				$result[] = [
					'NAME' => trim($item),
				];
			}
		}
		return $result;
	}
}
