<?php

namespace Astral\Ext\Helper;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\GroupTable;
use Bitrix\Main\SystemException;
use Exception;

class UserGroup
{
	/**
	 * @param string $code
	 * @param string $title
	 *
	 * @return int
	 * @throws ArgumentException
	 * @throws SystemException
	 * @throws Exception
	 */
	public function create(string $code, string $title): int
	{
		$managerGroup = GroupTable::query()
			->where('STRING_ID', $code)
			->setSelect(['ID'])
			->fetch();
		$managerGroupId = null;
		if ($managerGroup === false)
		{
			$newGroup = GroupTable::add([
				'ACTIVE' => 'Y',
				'C_SORT' => 1100,
				'NAME' => $title,
				'DESCRIPTION' => '',
				'STRING_ID' => $code,
			]);
			if ($newGroup->isSuccess())
			{
				$managerGroupId = $newGroup->getId();
			}
		} else
		{
			$managerGroupId = $managerGroup['ID'];
		}
		return (int) $managerGroupId;
	}
}
