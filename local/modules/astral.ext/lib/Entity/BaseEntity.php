<?php

namespace Astral\Ext\Entity;

use Bitrix\Crm\Model\Dynamic\TypeTable;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use Exception;
use RuntimeException;

abstract class BaseEntity
{
	/**
	 * @return array
	 *
	 * @throws ArgumentException
	 * @throws LoaderException
	 * @throws ObjectPropertyException
	 * @throws SystemException
	 * @throws Exception
	 */
	public static function create(): array
	{
		$result = [];
		if (Loader::includeModule('crm'))
		{
			$type = TypeTable::getList([
				'filter' => ['=NAME' => static::getTypeName()],
				'select' => ['ID', 'ENTITY_TYPE_ID', 'TABLE_NAME'],
			])->fetch();
			$arData = array_merge(
				[
					'ENTITY_TYPE_ID' => TypeTable::getNextAvailableEntityTypeId(),
					'NAME' => static::getTypeName(),
					'TABLE_NAME' => 'b_crm_dynamic_items_' . mb_strtolower(static::getTypeName()),
					'TITLE' => static::getTypeName(),
					'CODE' => static::getTypeName(),
					'CREATED_BY' => 1,
					'IS_CATEGORIES_ENABLED' => 'N',
					'IS_STAGES_ENABLED' => 'N',
					'IS_BEGIN_CLOSE_DATES_ENABLED' => 'N',
					'IS_CLIENT_ENABLED' => 'N',
					'IS_USE_IN_USERFIELD_ENABLED' => 'N',
					'IS_LINK_WITH_PRODUCTS_ENABLED' => 'N',
					'IS_CRM_TRACKING_ENABLED' => 'N',
					'IS_MYCOMPANY_ENABLED' => 'N',
					'IS_DOCUMENTS_ENABLED' => 'N',
					'IS_SOURCE_ENABLED' => 'N',
					'IS_OBSERVERS_ENABLED' => 'N',
					'IS_RECYCLEBIN_ENABLED' => 'N',
					'IS_AUTOMATION_ENABLED' => 'N',
					'IS_BIZ_PROC_ENABLED' => 'N',
					'IS_SET_OPEN_PERMISSIONS' => 'N',
				],
				static::getConfig()
			);
			if ($type === false)
			{
				$add = TypeTable::add($arData);
				if (!$add->isSuccess())
				{
					$result = $add->getErrorMessages();
				}
			} else
			{
				$arData['TABLE_NAME'] = $type['TABLE_NAME'];
				$arData['ENTITY_TYPE_ID'] = $type['ENTITY_TYPE_ID'];
				$update = TypeTable::update($type['ID'], $arData);
				if (!$update->isSuccess())
				{
					$result = $update->getErrorMessages();
				}
			}
		}
		return $result;
	}

	abstract public static function getTypeName(): string;

	abstract public static function getConfig(): array;

	/**
	 * @return string
	 *
	 * @throws ArgumentException
	 * @throws ObjectPropertyException
	 * @throws SystemException
	 */
	public static function getEntityCode(): string
	{
		$type = TypeTable::getList([
			'filter' => ['=NAME' => static::getTypeName()],
			'select' => ['ID'],
		])->fetch();
		if ($type === false)
		{
			throw new RuntimeException(static::getTypeName() . ' not exist');
		}
		return 'CRM_' . $type['ID'];
	}

	/**
	 * @return int
	 *
	 * @throws ArgumentException
	 * @throws ObjectPropertyException
	 * @throws SystemException
	 */
	public static function getDynamicTypeId(): int
	{
		$type = TypeTable::getList([
			'filter' => ['=NAME' => static::getTypeName()],
			'select' => ['ENTITY_TYPE_ID'],
		])->fetch();
		if ($type === false)
		{
			throw new RuntimeException(static::getTypeName() . ' not exist');
		}
		return (int) $type['ENTITY_TYPE_ID'];
	}
}
