<?php

namespace Astral\Ext\Orm;

use Bitrix\Main\ORM\Data\AddResult;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Data\UpdateResult;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\SystemException;
use RuntimeException;

class EventsTable extends DataManager
{
	public static function getTableName(): string
	{
		return 'b_module_to_module';
	}

	/**
	 * @return array
	 *
	 * @throws SystemException
	 */
	public static function getMap(): array
	{
		return [
			new IntegerField('ID', [
				'primary' => true,
				'autocomplete' => true,
			]),
			new StringField('TO_MODULE_ID', [
				'required' => true,
				'unique' => true,
			]),
		];
	}

	public static function add(array $data): AddResult
	{
		throw new RuntimeException('Method not implemented');
	}

	public static function update($primary, array $data): UpdateResult
	{
		throw new RuntimeException('Method not implemented');
	}
}
