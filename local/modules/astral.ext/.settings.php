<?php
return [
	'controllers' => [
		'value' => [
			'namespaces' => [
				'\\Astral\\Ext\\Controller' => 'api',
			],
		],
		'readonly' => true,
	],
];
