<?php

use Astral\Ext\Helper\ConfigEntityEditorManager;
use Astral\Ext\Helper\CrmStatusManager;
use Astral\Ext\Helper\IBlockManager;
use Astral\Ext\Helper\UserFieldsManager;
use Astral\Ext\Orm\EventsTable;
use Bitrix\Main\Application;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\ArgumentNullException;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;

Loc::loadMessages(__FILE__);

class astral_ext extends CModule
{
	/**
	 * ASTRAL_EXT constructor.
	 */
	public function __construct()
	{
		$arModuleVersion = [];
		include __DIR__ . '/version.php';

		$this->MODULE_ID = 'astral.ext';
		$this->MODULE_VERSION = $arModuleVersion['VERSION'] ?? '1.0.0';
		$this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'] ?? date('Y-m-d');
		$this->MODULE_NAME = Loc::getMessage('ASTRAL_EXT_MODULE_NAME');
		$this->MODULE_DESCRIPTION = Loc::getMessage('ASTRAL_EXT_MODULE_DESC');

		$this->PARTNER_NAME = Loc::getMessage('ASTRAL_EXT_PARTNER_NAME');
		$this->PARTNER_URI = Loc::getMessage('ASTRAL_EXT_PARTNER_URI');

		$this->MODULE_SORT = 1;
		$this->SHOW_SUPER_ADMIN_GROUP_RIGHTS = 'Y';
		$this->MODULE_GROUP_RIGHTS = 'Y';
	}

	/**
	 * @throws ArgumentException
	 * @throws ArgumentNullException
	 * @throws LoaderException
	 * @throws ObjectPropertyException
	 * @throws SystemException
	 */
	public function DoInstall(): void
	{
		global $APPLICATION;

		ModuleManager::registerModule($this->MODULE_ID);

		$this->InstallEvents();
		$this->InstallFiles();
		$this->InstallDB();

		$APPLICATION->IncludeAdminFile(
			Loc::getMessage('ASTRAL_EXT_INSTALL_TITLE'),
			$this->getPath() . '/install/step.php'
		);
	}

	public function InstallEvents(): void
	{
	}

	/**
	 * @throws ArgumentException
	 * @throws ArgumentNullException
	 * @throws LoaderException
	 * @throws ObjectPropertyException
	 * @throws SystemException
	 */
	public function InstallDB(): void
	{
		Loader::includeModule($this->MODULE_ID);
		IBlockManager::check();
		UserFieldsManager::check();
		ConfigEntityEditorManager::check();
		CrmStatusManager::check();
	}

	private function getPath(): string
	{
		return dirname(__DIR__);
	}

	/**
	 * @throws ArgumentNullException
	 * @throws LoaderException
	 * @throws SystemException
	 */
	public function DoUninstall(): void
	{
		Loader::includeModule($this->MODULE_ID);
		global $APPLICATION;
		/** @noinspection NullPointerExceptionInspection */
		$context = Application::getInstance()->getContext();
		$request = $context->getRequest();
		if ((int) $request['step'] < 2)
		{
			$APPLICATION->IncludeAdminFile(Loc::getMessage('ASTRAL_EXT_UNINSTALL_TITLE'), $this->getPath()
				. '/install/unstep1.php');
		} elseif ((int) $request['step'] === 2)
		{
			$this->UnInstallFiles();
			$this->UnInstallEvents();
			if ($request['savedata'] !== 'Y')
			{
				$this->UnInstallDB();
			}
			ModuleManager::unRegisterModule($this->MODULE_ID);
			$APPLICATION->IncludeAdminFile(Loc::getMessage('ASTRAL_EXT_UNINSTALL_TITLE'), $this->getPath()
				. '/install/unstep2.php');
		}
	}

	public function UnInstallFiles(): void
	{
	}

	/**
	 * @throws ArgumentException
	 * @throws ObjectPropertyException
	 * @throws SystemException
	 * @throws Exception
	 */
	public function UnInstallEvents(): void
	{
		$ev = EventsTable::getList([
			'filter' => ['TO_MODULE_ID' => $this->MODULE_ID],
		])->fetchAll();
		foreach ($ev as $item)
		{
			EventsTable::delete($item['ID']);
		}
	}

	/**
	 * @throws ArgumentNullException
	 * @throws LoaderException
	 */
	public function UnInstallDB(): void
	{
		Loader::includeModule($this->MODULE_ID);
		Option::delete($this->MODULE_ID);
	}

	/**
	 * @throws ArgumentException
	 * @throws LoaderException
	 * @throws ObjectPropertyException
	 * @throws SystemException
	 */
	public function reInstall(): void
	{
		$this->InstallDB();
		$this->UnInstallEvents();
		$this->InstallEvents();
	}
}
