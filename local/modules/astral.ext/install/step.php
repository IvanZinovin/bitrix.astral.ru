<?php

use Bitrix\Main\Localization\Loc;

if (!check_bitrix_sessid())
{
	return;
}
global $APPLICATION;

$ex = $APPLICATION->GetException();
if ($ex !== false)
{
	$adminMessage = new CAdminMessage(Loc::getMessage('MOD_INST_ERR'), $ex);
	echo $adminMessage->Show();
} else
{
	CAdminMessage::ShowNote(Loc::getMessage('MOD_INST_OK'));
}
?>
<form action="<?= $APPLICATION->GetCurPage() ?>">
	<input type="hidden" name="lang" value="<?= LANGUAGE_ID ?>">
	<input type="submit" name="" value="<?= Loc::getMessage('MOD_BACK') ?>">
</form>
