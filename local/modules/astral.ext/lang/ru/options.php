<?php

$MESS['DEPENDENCY_UPDATE'] = 'Обновить зависимости';
$MESS['ASTRAL_EXT_TAB_SETTINGS'] = 'Основные настройки';
$MESS['ASTRAL_EXT_SETTINGS_TITLE'] = 'Настройка';
$MESS['ASTRAL_EXT_ALERT'] = 'Для каждой настройки должно быть прописано значение по умолчанию!';
$MESS['SESSID_ERROR'] = 'Не корректный идентификатор сессии';
