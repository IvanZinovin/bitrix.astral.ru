<?php

if (!defined('B_PROLOG_INCLUDED'))
{
	die();
}

if (file_exists(dirname(__DIR__, 2) . '/bitrix/php_interface/init.php'))
{
	/** @noinspection PhpIncludeInspection */
	require_once dirname(__DIR__, 2) . '/bitrix/php_interface/init.php';
}

if (file_exists(dirname(__DIR__) . '/vendor/autoload.php'))
{
	require_once dirname(__DIR__) . '/vendor/autoload.php';
}

if (file_exists(__DIR__ . '/dev.php'))
{
	require_once __DIR__ . '/dev.php';
}

const CRM_USE_CUSTOM_SERVICES = true;
if (defined('CRM_USE_CUSTOM_SERVICES') && CRM_USE_CUSTOM_SERVICES === true)
{
	$fileName = __DIR__ . '/include/crm_services.php';
	if (file_exists($fileName))
	{
		require_once($fileName);
	}
}
