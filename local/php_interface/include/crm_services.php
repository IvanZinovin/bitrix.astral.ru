<?php

use Bitrix\Main\Loader;

if (!defined('B_PROLOG_INCLUDED'))
{
	die();
}

try
{
	if (Loader::includeModule('astral.ext')
		&& Loader::includeModule('crm'))
	{
		//Logic
	}
} catch (Exception $exception)
{
	ShowError($exception->getMessage());
}

